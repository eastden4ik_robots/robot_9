package ru.sviridoff.robots;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sviridoff.framework.driver.SeleniumDriver;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;

import java.io.IOException;

public class robot_9 {


    private static Logger logger = new Logger();
    public static final String logs = "./log.txt";
    private static Props props;
    public static SeleniumDriver seleniumDriver = new SeleniumDriver();
    public static WebDriver driver;

    static {
        try {
            props = new Props("props.properties");
        } catch (IOException ex) {
            logger.Logging(Logger.LogLevel.DANGER, ex.getMessage(), logs);
        }
    }

    /*
    * MAIN:
    *   Количество попыток войти по паролю ограничено 10 попытками, возможно меньше.
    * */

    public static void main(String[] args) {

        logger.Logging(Logger.LogLevel.INFO, "Robot 9 is running ... ", logs);
        driver = seleniumDriver.Init(SeleniumDriver.Drivers.CHROME_HEADLESS);

        // Get uri page
        WebDriverWait wait = new WebDriverWait(driver, 40);
        driver.get(props.getProperty("megafon.url"));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(Xpaths.XPATH_LOGIN))));
        driver.findElement(By.xpath(Xpaths.XPATH_LOGIN)).sendKeys(props.getProperty("megafon.login"));
        driver.findElement(By.xpath(Xpaths.XPATH_PASSWORD)).sendKeys(props.getProperty("megafon.password"));
        driver.findElement(By.xpath(Xpaths.XPATH_LOGIN_BTN)).click();

        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(Xpaths.XPATH_LOGIN_DASHBOARD))));
        String siteLogin = driver.findElement(By.xpath(Xpaths.XPATH_LOGIN_DASHBOARD)).getText().replace(" ", "");
        String propsLogin = "+" + props.getProperty("megafon.login");
        if (siteLogin.equals(propsLogin)) {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xpaths.XPATH_MONEY_AMOUNT + "/h4")));
//            String phrase = driver.findElement(By.xpath(Xpaths.XPATH_MONEY_AMOUNT + "/h4")).getText();
            String balance = driver.findElement(By.xpath(Xpaths.XPATH_MONEY_AMOUNT + "/h3")).getText();
            logger.Logging(Logger.LogLevel.SUCCESS, "На счету: " + balance + " .", logs);
        } else {
            logger.Logging(Logger.LogLevel.DANGER, "Login: " + propsLogin + ", From site: " + siteLogin + ". ", logs);
        }
        seleniumDriver.Destroy(driver);


    }



}
