package ru.sviridoff.robots;

public class Xpaths {

    public final static String XPATH_LOGIN = "//input[contains(@name,'j_username')]";
    public final static String XPATH_PASSWORD = "//input[contains(@name,'j_password')]";
    public final static String XPATH_LOGIN_BTN = "/html/body/div[3]/div[1]/div/div/div/div[2]/div[3]/form/div[3]/div/span/span";
    public final static String XPATH_LOGIN_DASHBOARD = "//div[contains(@class,'lk_header_info')]/div[1]/p";
    public final static String XPATH_MONEY_AMOUNT = "//div[contains(@class,'gadget_account')]/div[1]";

}